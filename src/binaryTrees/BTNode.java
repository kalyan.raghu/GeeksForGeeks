package binaryTrees;

public class BTNode {
	
	private BTNode leftChild;
	private BTNode rightChild;
	
	private int data;
	
	public BTNode(int data) {
		this.data = data;
		this.leftChild = null;
		this.rightChild = null;
	}
	
	public void addLeftChild(BTNode node) {
		this.leftChild = node;
	}
	
	public void addRightChild(BTNode node) {
		this.rightChild = node;
	}
	
	public BTNode getLeftChild() {
		return leftChild;
	}
	
	public void setLeftChild(BTNode child) {
		this.leftChild = child;
	}
	
	public BTNode getRightChild() {
		return rightChild;
	}
	
	public void setRightChild(BTNode child) {
		this.rightChild = child;
	}
	
	public int getData() {
		return data;
	}

}
