package binaryTrees;

public class BinarySearchTree {

	private BTNode root;
	
	public BinarySearchTree() {
		this.root = null;
	}
	
	public BinarySearchTree(int data) {
		this.root = new BTNode(data);
	}
	
	public void addData(int data) {
		if(this.root == null) {
			this.root = new BTNode(data);
		}
		else {
			if(data < this.root.getData()) {
				this.root.setLeftChild(addData(this.root.getLeftChild(), data));
			}
			else {
				this.root.setRightChild(addData(this.root.getRightChild(), data));
			}
		}
	}
	
	public BTNode addData(BTNode node, int data) {
		if(node == null) {
			node = new BTNode(data);
			return node;
		}
		else {
			if(data < node.getData()) {
				node.setLeftChild(addData(node.getLeftChild(), data));
			}
			else {
				node.setRightChild(addData(node.getRightChild(), data));
			}
			
			return node;
		}
	}
	
	
}
